<?php
/*
 * Name:    example.php
 * Author:  Justin Fingerhuth (justin.fingerhuth@fingerhuth-it.de)
 * Version: 1.0
 * Created: 2021-01-15
 * License: MIT License (see README.md)
 * Requires:API-Key of PeDePe's-BCS
 */
# MySQL Access
$username = "";  # DB User here
$password = "";  # DB Password here
$database = "";  # DB Name here
$dbserver = "";  # DB Server here
$mysqli_datalink = mysqli_connect($dbserver, $username, $password, $database);
mysqli_set_charset($mysqli_datalink, 'utf8');

# Include the PHP-Class and establish connection to API
require("PeDePe.class.php");

Try {
    $pedepe = new PeDePe("YOUR_KEY");   # Place your API-Key here
} catch (Exception $e) {
    echo "ERROR: ".$e->getMessage();
}
# Let's get and print some basic data of our company
echo '<br>Hello, User! My Name is '.$pedepe->CompanyName.'. I have been founded at '.$pedepe->CompanyFoundation.' and owned by '.$pedepe->CompanyOwners.'<br><br>';

# Let's get all of our maps and add them to our database. If you do not want this feature, just set a # in front of Line 31
try {
    $pedepe->AddMapsToDatabase($mysqli_datalink);
} catch (Exception $e) {
    echo $e->getMessage();
}

# Let's add the company statistics to our database. If you do not want this feature, just set a # in front of Line 39
# It is not recommended to use this feature, but if you want to, you can do it ;-)
try {
    $pedepe->AddStatisticsToDB($mysqli_datalink);
} catch (Exception $e) {
    echo $e->getMessage();
}

# Let's add our buses to our database. If you do not want this feature, just set a # in front of Line 46
try {
    $pedepe->AddBusesToDB($mysqli_datalink);
} catch (Exception $e) {
    echo $e->getMessage();
}

# Let's print the price for Berlin-Spandau
# The name you provide has to be exactly like the name in BCS! Otherwise your script will maybe end here...
try {
    echo $pedepe->GetMapPrice("Berlin-Spandau");
} catch (Exception $e) {
    echo $e->getMessage();
}

# Let's add all employees to our database. If you do not want this feature, just set a # in front of Line 61
try {
    $pedepe->AddStaffToDB($mysqli_datalink);
} catch (Exception $e) {
    echo $e->getMessage();
}
