# PeDePe-APIHelper #
Dieser kleine Helfer erlaubt das unkomplizierte Abfragen der PeDePe-BBS-API, ohne ständig mit Curl zu arbeiten.

### Installation ###
Um die API zu nutzen, wird zunächst ein API-Key benötigt.
Gehe hierzu in den BBS, dort im Multiplayer (!) an einen Computer. Wechsle nun in den Betriebsmodus und wähle den Punkt "Einstellungen" (da, wo Du auch die Lohnabgabe einstellen kannst). Scrolle hier nun nach ganz unten und klicke bei API auf "Informationen". Hier wird Dir nun Dein API-Key angezeigt.

Nun musst Du nur noch, diesen Helfer herunterladen (git clone) und in die eignen Scripte einfügen. Ein Beispiel findest Du im example.php

**Wichtig:** Um die Funktionalität der Karten in der API zu nutzen, muss folgendes Script zwingend in die eigene MySQL-Datenbank importiert werden (bitte nichts umbenennen oder anpassen!):

    CREATE TABLE IF NOT EXISTS `buses`  (
    `id` int(11) NOT NULL,
    `depot` int(2) NOT NULL,
    `parkingSpace` int(2) NOT NULL,
    `inWorkshop` varchar(10) NOT NULL DEFAULT 'false',
    `parkingSpaceWorkshop` int(2) NOT NULL,
    `mileage` varchar(45) NOT NULL,
    `licensePlate` varchar(45) NOT NULL,
    `name` varchar(90) NOT NULL,
    `brand` varchar(90) NOT NULL,
    `repaintCompanyPremises` varchar(90) NOT NULL,
    `repaintOmsi` varchar(90) NOT NULL,
    `folderName` varchar(90) NOT NULL,
    `fuelPercent` int(3) NOT NULL,
    `lastOilChangement` double NOT NULL,
    `nextOilChangement` double NOT NULL,
    `lastTyreChange` double NOT NULL,
    `nextTyreChange` double NOT NULL,
    `lastVbeltChange` double NOT NULL,
    `nextVbeltChange` double NOT NULL,
    `lastGeneralInspection` datetime NOT NULL,
    `nextGeneralInspection` datetime NOT NULL,
    `purchaseTime` datetime NOT NULL,
    `purchasePrice` int(11) NOT NULL,
    `articulatedBus` varchar(10) NOT NULL DEFAULT 'false',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    CREATE TABLE IF NOT EXISTS `company_statistics` (
    `id` int(11) NOT NULL,
    `company_name` varchar(90) NOT NULL,
    `company_language` varchar(3) NOT NULL,
    `company_owners` varchar(90) NOT NULL,
    `company_foundation` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `company_balance` int(11) NOT NULL,
    `company_lps` int(11) NOT NULL,
    `company_buses` int(3) NOT NULL,
    `company_employees` int(5) NOT NULL,
    `company_rank` int(2) NOT NULL,
    `company_repairshares` int(3) NOT NULL,
    `company_wagelevy` int(3) NOT NULL,
    `company_rating` double NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    CREATE TABLE IF NOT EXISTS `maps` (
    `mapname` varchar(90) NOT NULL,
    `startedTours` int(4) NOT NULL DEFAULT 0,
    `completedTours` int(4) NOT NULL DEFAULT 0,
    `revenue` int(11) NOT NULL DEFAULT 0,
    `punctuality` float NOT NULL DEFAULT 0,
    `trafficAccidents` int(4) NOT NULL DEFAULT 0,
    `concessionValidUntil` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
    `lastUpdate` timestamp NOT NULL DEFAULT current_timestamp(),
    `enabled` int(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (`mapname`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    CREATE TABLE IF NOT EXISTS `staff` (
    `employeeId` int(11) NOT NULL,
    `username` varchar(90) NOT NULL,
    `title` varchar(90) DEFAULT NULL,
    `lastCompletedTour` datetime NOT NULL,
    `revenue` int(11) NOT NULL,
    `revenueLast30Days` int(11) NOT NULL,
    `repairCosts` int(11) NOT NULL,
    PRIMARY KEY (`employeeId`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

### Wie starte ich die API? ###

    $pedepe = New PeDePe(<YOUR API KEY>);   # Erzeugt eine neue API-Instanz. Der API-Key wird ZWINGEND benötigt

### Welche Funktionen gibt es? ###
    $pedepe->AddMapsToDatabase($MySQLi_DataLink);

Diese Funktion erzeugt für jede im Betrieb vorhandene Karte einen Eintrag in der Datenbank. Die entsprechende Tabelle muss zuvor angelegt werden.

_Weitere Funktionen sind in Arbeit und folgen._

### Welche Variablen stehen zur Verfügung? ###
Informationen aus der Klasse werden mit folgender Syntax abgefragt:

    $pedepe->CompanyName;   # $ClassName->VarName;
Es stehen folgende Variablen zur Verfügung:

    CompanyName             Name der Firma im BBS
    CompanyLanguage         Sprachcode der Firma im BBS (deu, eng etc.)
    CompanyOwners           Liste der Betriebsinhaber (Kommasepariert)
    CompanyFoundation       Gründungsdatum der Firma im Format dd.mm.YYYY HH:MM
    CompanyAccountBalance   Aktueller Kontostand der Firma im BBS
    CompanyLPCount          Anzahl der LP, welche die Firma für den LSS zur Verfügung hat
    NumberBuses             Anzahl der Busse, welche der Firma gehören
    NumberEmployees         Anzahl der angestellten Mitarbeiter
    RequiredRank            Benötigter Rang zum Beitritt der Firma
    IsPrivate               True wenn der Betrieb nicht öffentlich ist, False wenn er öffentlich ist
    IsConnected             True wenn der Betrieb mit anderen Betrieben verbunden ist, False wenn nicht
    RepairShares            Reparaturabgabe in Prozent
    WageLevy                Lohnabgabe in Prozent
    TimeShift               Zeitverschiebung in Stunden
    CompanyRating           Betriebsbewertung in Prozent (auf 4 Stellen)

### Fragen? Anregungen? Wünsche? ###
Gerne per Mail an justin.fingerhuth@fingerhuth-it.de

### Lizenz ###
Das Ganze hier wurde unter der MIT-Lizenz veröffentlicht und darf somit unter Verweis auf dieses GIT-Repo verbreitet werden.

**MIT License**

Copyright (c) 2021 - Justin Fingerhuth

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.