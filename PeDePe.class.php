<?php
/*
 * Name:    PeDePe.class.php
 * Author:  Justin Fingerhuth (justin.fingerhuth@fingerhuth-it.de)
 * Version: 1.0
 * Created: 2021-01-15
 * License: MIT License (see README.md)
 * Requires:API-Key of PeDePe's-BCS
 */

class PeDePe
{
    private ?array $AllMaps = null;
    public string $CompanyName = "";
    public string $CompanyLanguage = "";
    public string $CompanyOwners = "";
    public string $CompanyFoundation = "";
    public int $CompanyAccountBalance = 0;
    public int $CompanyLPCount = 0;
    public int $NumberBuses = 0;
    public int $NumberEmployees = 0;
    public int $RequiredRank = 1;
    public bool $IsPrivate = false;
    public bool $IsConncted = false;
    public int $RepairShares = 0 ;
    public int $WageLevy = 0;
    public int $TimeShift = 0;
    public float $CompanyRating = 0.0;
    private string $apiKey = "";
    private string $apiURL = "https://pedepe.de/api/api.php?key=";

    /**
     * @param string $apiKey Provide the API-Key from PeDePe to query API
     * @throws Exception
     */
    public function __construct(string $apiKey)
    {
        if ($apiKey == "" OR !isset($apiKey)) {
            throw new Exception("API Key is missing");
        }
        $this->apiKey = $apiKey;
        $this->AuthToAPI();

        $company = json_decode($this->QueryAPI("company"));
        $this->CompanyName = $company->name;
        $this->CompanyLanguage = $company->language;
        $this->CompanyOwners = $company->owners;
        $this->CompanyFoundation = date("d.m.Y H:i", $company->foundation / 1000);
        $this->CompanyAccountBalance = $company->accountBalance;
        $this->CompanyLPCount = $company->lp;
        $this->NumberBuses = $company->numberOfBuses;
        $this->NumberEmployees = $company->numberOfEmployees;
        $this->RequiredRank = $company->neededRank;
        if ($company->privat == "false") {
            $this->IsPrivate = false;
        } else {
            $this->IsPrivate = true;
        }
        if ($company->connected == "false") {
            $this->IsConncted = false;
        } else {
            $this->IsConncted = true;
        }
        $this->RepairShares = $company->repairShare;
        $this->WageLevy = $company->wageLevy;
        $this->CompanyRating = $company->rating;

        #$mapresult = json_decode($this->QueryAPI("mapStatistics"));


        #$this->AllMaps = json_decode($result);
    }

    /**
     * This Function fills the "AllMaps"-Variable with content.
     * It is a separate function due to the runtime of this function on API-side.
     * Please use only if necessary!
     */
    protected function GetMaps() {
        $this->AllMaps = json_decode($this->QueryAPI("mapStatistics"));
    }

    /**
     * @param mysqli $MySQLi_DataLink A Datalink for MySQL (We'll query the database for you!)
     * @return bool
     * @throws Exception
     */
    public function AddMapsToDatabase(mysqli $MySQLi_DataLink): bool {
        if ($this->AllMaps == null) {
            $this->GetMaps();
        }
        # Get all in DB first
        $sql_all = "SELECT mapname FROM maps";
        $go_all = mysqli_query($MySQLi_DataLink, $sql_all);
        $map_in_db = mysqli_fetch_array($go_all, MYSQLI_ASSOC);
        $sql = "DELETE FROM maps WHERE enabled < 9"; # All Maps Disabled
        mysqli_query($MySQLi_DataLink, $sql);
        foreach ($this->AllMaps as $map) {
            $concession = date("Y-m-d H:i:s", $map->concessionValidUntil / 1000);
            $sql = "INSERT INTO maps VALUES (
              '$map->name',
              $map->startedToursLast30Days,
              $map->completedToursLast30Days,
              $map->revenueLast90Days,
              $map->punctuality,
              $map->trafficAccidents,
              '$concession',
              NOW(),
              1)";
            $go = mysqli_query($MySQLi_DataLink, $sql);
            if (!$go) {
                Throw New Exception("Error while adding $map->name to Database");
            }
        }
        return true;
    }

    /**
     * @param mysqli $MySQLi_DataLink A Datalink for MySQL (We'll query the database for you!)
     * @return bool
     * @throws Exception
     */
    public function AddStatisticsToDB(mysqli $MySQLi_Datalink): bool {
        $sql_all = "DELETE FROM company_statistics WHERE id = 1";
        mysqli_query($MySQLi_Datalink, $sql_all);

        $foundation = strtotime($this->CompanyFoundation.':00');
        $foundation = date("Y-m-d H:i:s", $foundation);

        $sql = "INSERT INTO company_statistics VALUES(
                    1,
                    '$this->CompanyName',
                    '$this->CompanyLanguage',
                    '$this->CompanyOwners',
                    '$foundation',
                    $this->CompanyAccountBalance,
                    $this->CompanyLPCount,
                    $this->NumberBuses,
                    $this->NumberEmployees,
                    $this->RequiredRank,
                    $this->RepairShares,
                    $this->WageLevy,
                    $this->CompanyRating)";
        $go = mysqli_query($MySQLi_Datalink, $sql);
        if (!$go) {
            Throw New Exception("Error while adding company statistics to database");
        }
        return true;
    }

    public function AddBusesToDB(mysqli $MySQLi_Datalink): bool {
        $sql_all = "DELETE FROM buses WHERE employeeId > 0";
        mysqli_query($MySQLi_Datalink, $sql_all);
        $businfos = json_decode($this->QueryAPI("buses"));
        foreach ($businfos as $bus) {
            $lastGeneralInspection = date("Y-m-d H:m:i", ($bus->lastGeneralInspection / 1000));
            $nextGeneralInspection = date("Y-m-d H:m:i", ($bus->nextGeneralInspection / 1000));
            $purchaseTime = date("Y-m-d H:m:i", ($bus->purchaseTime / 1000));
            $sql = "INSERT INTO buses VALUES (
                          $bus->id,
                          $bus->depot,
                          $bus->parkingSpace,
                          '$bus->inWorkshop',
                          $bus->parkingSpaceWorkshop,
                          '$bus->mileage',
                          '".mysqli_real_escape_string($MySQLi_Datalink, $bus->licensePlate)."',
                          '".mysqli_real_escape_string($MySQLi_Datalink, $bus->name)."',
                          '".mysqli_real_escape_string($MySQLi_Datalink, $bus->brand)."',
                          '".mysqli_real_escape_string($MySQLi_Datalink, $bus->repaintCompanyPremises)."',
                          '".mysqli_real_escape_string($MySQLi_Datalink, $bus->repaintOmsi)."',
                          '".mysqli_real_escape_string($MySQLi_Datalink, $bus->folderName)."',
                          '$bus->fuelPercent',
                          '$bus->lastOilChangement',
                          '$bus->nextOilChangement',
                          '$bus->lastTyreChange',
                          '$bus->nextTyreChange',
                          '$bus->lastVbeltChange',
                          '$bus->nextVbeltChange',
                          '$lastGeneralInspection',
                          '$nextGeneralInspection',
                          '$purchaseTime',
                          '$bus->purchasePrice',
                          '$bus->articulatedBus'
            )";
            $go = mysqli_query($MySQLi_Datalink, $sql);
            if (!$go) {
                Throw New Exception("Error while adding bus with license plate '$bus->licensePlate' to database. SQL: $sql");
            }
        }
        return true;
    }

    public function GetMapPrice(string $MapName): int {
        $mapPrices = json_decode($this->QueryAPI("mapPrices"));
        foreach ($mapPrices as $map) {
            if ($MapName == $map->name) {
                return $map->price;
            }
        }
        Throw New Exception("Map with name '$MapName' seems to be not supported by BCS. Please check if the provided name is exactly identical with the name in BCS.");
    }

    public function AddStaffToDB(mysqli $MySQLi_Datalink): bool {
        $sql_all = "DELETE FROM staff WHERE id > 0";
        mysqli_query($MySQLi_Datalink, $sql_all);
        $staffInfo = json_decode($this->QueryAPI("staff"));
        foreach ($staffInfo->employees as $staff) {

            $lastCompletedTour = date("Y-m-d H:m:i", ($staff->lastCompletedTour / 1000));
            $sql = "INSERT INTO staff VALUES(
                         $staff->employeeId,
                         '$staff->username',
                         '$staff->title',
                         '$lastCompletedTour',
                         $staff->revenue,
                         $staff->revenueLast30Days,
                         $staff->repairCosts
            )";
            $go = mysqli_query($MySQLi_Datalink, $sql);
            if (!$go) {
                Throw New Exception("Error while adding employee '$staff->username' to database");
            }
        }
        return true;
    }

    private function AuthToAPI(): bool {
        $out = $this->QueryAPI("company");
        if ($out == "") {
            Throw New Exception("API Key seems to be wrong");
        }
        return true;
    }

    /**
     * @param string $action Select an action for API (company, maps, mapStatistics)
     * @return string
     */
    private function QueryAPI(string $action): string {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiURL.$this->apiKey.'&request='.$action.'json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}